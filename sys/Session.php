<?php
final class Session {
    /**
  * funkija otvaranja sesije
  */
    public static final function begin(){
        session_start();
    }
      /**
  * funkija zatvaranja  i cistenja iz sesije
  */
    public static final function end(){
        self::clear();
        session_destroy();
    }
      /**
  * funkija validacije podataka
  */
    private static final function isValid($key){
        return preg_match('/^[A-z0-9_]*$/', $key);
    }
    public static final function set($key,$value){
        if(self::isValid($key)){
            $_SESSION[$key] = $value;
            return true;
        }else{
            return false;
        }
    }
      /**
  * funkija provere postojanja kljuca
  */
    public static final function exists($key){
        if(self::isValid($key)){
            return isset($_SESSION[$key]);
        }else{
            return false;
        }
    }
  /**
  * vracanje sesije sa kljucem
   * @return  $key
  */
    public static final function get($key){
        if(self::isValid($key) and self::exists($key)){
                return $_SESSION[$key];
        }else{
            return false;
        }
    }
       /**
  * funkija ciscenja iz sesije
  */
    public static final function clear(){
       $_SESSION = []; 
       return true;
    }
}
