<?php
 
    final class DataBase {
    
        private static $db = null;
        
         public static function getInstance() {
            if (self::$db === NULL) {                                          # Konektovanje se izvrsava samo 1
                self::$db = new PDO('mysql:host=' . Configuration::DB_HOST .  # Adresa servera
                                      ';dbname=' . Configuration::DB_BASE .     # Ime baze podataka
                                      ';charset=utf8',                          # Unikodna podrska
                                      Configuration::DB_USER,                   # Korisnik za bazu
                                      Configuration::DB_PASS);                  # Lozinka tog korisnika
                self::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);   # Ne zelimo da server testira izraze
            }

            return self::$db; # Vracamo primerak konekcije na bazu podatala
        }
}
