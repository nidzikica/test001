<?php
   /**
  * funkija __autoload za pomoc ucitavanju klasa
  */
    function __autoload($imeKlase){
    if ($imeKlase === 'Misc' or $imeKlase === 'DataBase' or $imeKlase === 'Controller' or $imeKlase === 'ModelInterface' or $imeKlase === 'Session' or $imeKlase === 'AdminController') {
        require_once 'sys/' . $imeKlase .'.php';        
    }elseif(preg_match('/^([A-Z][a-z]+)+Controller$/', $imeKlase)){
        require_once 'app/controllers/'. $imeKlase .'.php';
    }elseif (preg_match('/^([A-Z][a-z]+)+Model$/', $imeKlase)){
        require_once 'app/models/' . $imeKlase . '.php';
      } elseif ($imeKlase ==='Configuration') {
          require_once  $imeKlase . '.php';
    
}
}
