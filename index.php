<?php

#Ucitvanjae autoloader funkije koja ce pomici pri ucitvanaju klasa prilikom njihove prve upotrebe
  require_once 'sys/Autoloader.php';
     #Startovanje sesije
  Session::begin();
  
  $Request = $_SERVER['REQUEST_URI'];
  #
  $Request = substr($Request, strlen(Configuration::PATH));
  #Ucitavanje definisanih ruta za ovu app
  $Routes= require_once './Routes.php';    
  #Nalazenje odrgovarajuce rute
  $Arguments=[];
  $FoundRoute=null;
  foreach ($Routes as $Route){
      if(preg_match($Route['Pattern'], $Request,$Arguments)){
          $FoundRoute = $Route;
          break;
          
      }
  }
  unset($Arguments[0]);
  $Arguments= array_values($Arguments);
 
  #Ucitavanje kontrolera za izabranu rutu
$controllerPath = 'app/controllers/' . $FoundRoute['Controller'] . 'Controller.php';
if(!file_exists($controllerPath)){
    die('Controller class does not exist.');
}

require_once $controllerPath;
#instaciranje kontrolera
$imeKlase = $FoundRoute['Controller'] . 'Controller';
$worker = new $imeKlase;
#eventualna izvrsavanje specijalnog metoda __pre
if(method_exists($worker, '__pre')){
    call_user_func([$worker, '__pre']);
}
if (method_exists($worker,  $FoundRoute['Method'])){
$methodName = $FoundRoute['Method'];
call_user_method_array($methodName, $worker, $Arguments);
}else{
    die('This controller does not have the requested method.');
}
$DATA = $worker->getData();
require 'app/views/' . $FoundRoute['Controller'] . '/' . $FoundRoute['Method'] . '.php';          
