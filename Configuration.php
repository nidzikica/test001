<?php
 /**
  * Konfiguracija sa konstantama za spajanje na bazu podataka 
  */
final class Configuration {
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = 'root';
    const DB_BASE = 'petproject';
    
     const BASE = 'http://localhost/ProjectUdomi/';
     const  PATH = '/ProjectUdomi/';
      
     const  SALT = '23232fddasdDSAsad231c1';
     const ITEM_PER_PAGE=  10;
     
     const IMAGE_DATA_PATH = 'data/image/';
     
}
