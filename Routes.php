<?php
   return [
       [
            'Pattern'    => '|^login/?$|',
            'Controller' => 'Main',
            'Method'     => 'login'
            
       ],
        [
            'Pattern'    => '|^registration/?$|',
            'Controller' => 'Main',
            'Method'     => 'registration'
            
       ],
        [
            'Pattern'    => '|^logout/?$|',
            'Controller' => 'Main',
            'Method'     => 'logout'
            
       ],
        [
            'Pattern'    => '|^admin/categories/?$|',
            'Controller' => 'AdminPetCategory',
            'Method'     => 'index'
            
       ]
       ,
        [
            'Pattern'    => '|^admin/categories/add/?$|',
            'Controller' => 'AdminPetCategory',
            'Method'     => 'add'
            
       ],
       [
            'Pattern'    => '|^pet/list/?$|',
            'Controller' => 'Pet',
            'Method'     => 'index'
        ],
         [
            'Pattern'    => '|^pet/edit/([0-9]+)/?$|',
            'Controller' => 'Pet',
            'Method'     => 'edit'
        ],
         [
            'Pattern'    => '|^images/pet/([0-9]+)/?$|',
            'Controller' => 'AdminImage',
            'Method'     => 'listPetImages'
        ],
       [
            'Pattern'    => '|^images/pet/([0-9]+)/add/?$|',
            'Controller' => 'AdminImage',
            'Method'     => 'uploadImage'
        ],
       
        [
            'Pattern'    => '|^pet/delete/([0-9]+)/?$|',
            'Controller' => 'Pet',
            'Method'     => 'delete'
        ],
       [
            'Pattern'    => '|^pet/add/?$|',
            'Controller' => 'Pet',
            'Method'     => 'add'
        ],
         [
            'Pattern'    => '|^pet/edit/([0-9]+)/?$|',
            'Controller' => 'Pet',
            'Method'     => 'edit'
        ],
         [
            'Pattern'    => '|^category/([a-z0-9\-]+)/?$|',
            'Controller' => 'Main',
            'Method'     => 'listByCategory'
            
       ],
      
         [
            'Pattern'    => '|^race/([a-z0-9\-]+)/?$|',
            'Controller' => 'Main',
            'Method'     => 'petRace'
            
       ] ,
        [
            'Pattern'    => '|^pet/([0-9]+)?/?$|',
            'Controller' => 'Main',
            'Method'     => 'petDetails'
        ],
       [
            'Pattern'    => '|^([0-9]+)?/?$|',
            'Controller' => 'Main',
            'Method'     => 'index'
        ],
       [
            'Pattern'    => '|^search/?$|',
            'Controller' => 'Main',
            'Method'     => 'search'
        ],
        [
            'Pattern'    => '|^.*$|',
            'Controller' => 'Main',
            'Method'     => 'index'
        ]
    ];