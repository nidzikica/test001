<!doctype html>
<html>
    <head>
        <title><?php echo @$DATA['seo_title']; ?></title>
        <link rel='icon' href="<?php echo Configuration::BASE; ?>assets/img/png.png"> 

        <meta charset="utf-8">
        <link href="<?php echo Configuration::BASE; ?>assets/css/main.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/main<?php echo $FoundRoute['Controller']; ?>.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel="stylesheet" >
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap-theme.min.css" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display+SC" rel="stylesheet"> 

        <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Monoton" rel="stylesheet">

    </head>
    <body class="colorback">
        

        <div class="container">
            <div class="row polariod-green"> 
                <nav id="navbar">
                               
                    
                       <ul class=" nav navbar-nav color  navbar-right clas">
                  
                           <li> <?php Misc::url('', 'Pocetna'); ?>  </li>
                       

                            <?php if (Session::exists('user_id')): ?>
                            <li> <?php Misc::url('pet/list', 'Vasi oglasi'); ?> </li>
                                <li> <?php Misc::url('pet/add', 'Novi oglas'); ?> </li>
                                <li><?php Misc::url('logout', 'Izloguj se'); ?> </li>
                            <?php else: ?>

                                <li> <?php Misc::url('login', 'Uloguj se'); ?> </li>
                                <li> <?php Misc::url('registration', 'Napravi nalog'); ?> </li>
                            <?php endif; ?> 
                                <a class="marg" href="<?php echo Configuration::BASE; ?>">    <img alt="udomi" src="<?php echo Configuration::BASE; ?>assets/img/udomi.png"></a>  
                   
                        </ul>
                    
                </nav>

            </div>