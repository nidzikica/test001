<?php include 'app/views/_global/beforeContent.php'; ?>
 

<article class="row  polariod">
    <div class="col-xs-12">
        <header>
            <h2 class="text-center color">Svi ljubimaci vrste  &quot;<?php echo htmlspecialchars($DATA['category']->name);?>&quot;</h2>
        </header>
        <div class="page-content">
            <div class="row">
                
                 <div class="col-md-9">
                     
                     <?php foreach ($DATA['pets'] as $pet): ?>
                     <div class="polariod pet">
                         <img src="<?php echo Configuration::BASE . $pet->images[0]->path; ?>" alt="<?php echo htmlspecialchars($pet->title); ?>" class="img-responsive">
                         <h2><?php echo htmlspecialchars($pet->title); ?></h2>
                         <hr>
                         <div class="pet-short-text">
                             <?php echo htmlspecialchars($pet->short_text); ?>
                         </div>
                        
                         <a class="button" href="<?php echo Configuration::BASE; ?>pet/<?php echo $pet->pet_id; ?>">Opsirnije</a>
                     </div>
                     <?php endforeach; ?>
                
                </div>
            </div>
        </div>
    </div>
</article>




<?php include 'app/views/_global/afterContent.php'; ?>

