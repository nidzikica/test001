<?php include 'app/views/_global/beforeContent.php'; ?>
 

<article class="row  polariod">
    <div class="col-xs-12">
        <header>
            <h2 class="text-center color">Svi ljubimaci na usvajanje</h2>
        </header> 
      
            <hr>
            <br>
        <div class="page-content">
            <div class="row ">
                <div class="col-sm-12 col-md-3">
                      <form method="post" class="form" action="<?php echo Configuration::BASE; ?>search">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <h3>Pretraga:</h3>
            </div>
                
                <div class="col-sm-12 ">
                      <div class="form-group">
                       
                          <label>Vrsta:</label><select class="form-control" name="pet_category_id">
                            <option value="-1">- - -</option>
                            <?php foreach ($DATA['categories'] as $category):?>
                            <option value="<?php echo $category->pet_category_id; ?>"><?php echo htmlspecialchars($category->name);?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>  
                    
                    <div class="form-group">
                        <label>Rasa:</label>
                        <select class="form-control" name="race_id">
                            <option value="-1">- - -</option>
                            <?php foreach ($DATA['races'] as $race):?>
                            <option value="<?php echo $race->race_id; ?>"><?php echo htmlspecialchars($race->name);?></option>
                            <?php endforeach; ?>
                        </select>
                   
                </div>
                </div>
                 <div class="col-sm-12 ">
                     <form class="form-group">
                         <label>Tagovi:</label>
                         <br><div class="polariod">
                     <?php foreach ($DATA['tags'] as $tag): ?>
                         <label class="checkbox text-left margin">
                         <input type="checkbox" name="tag_ids[]" value="<?php echo $tag->tag_id; ?>">
                         <?php echo htmlspecialchars($tag->name); ?>
                         </label>
                         <?php endforeach; ?>
                   
                        
                         </div> <hr>
                 </div>
                 <div class="col-sm-12">
                     <button type="submit" class="btn btn-primary form-control">Pretrazi</button>
                 </div>
            </div>   </form>
                </div>
                 <div class="col-md-9">
                     
                     <?php foreach ($DATA['pets'] as $pet): ?>
                     <article class="row polariod">
                         <div class="col-sm-12 col-md-6">
                         <img src="<?php echo Configuration::BASE . $pet->images[0]->path; ?>" alt="<?php echo htmlspecialchars($pet->title); ?>"  class="img-responsive grayscale">
                         </div>
                                                  <div class="col-sm-12 col-md-6">
                                                      <h2 class="color">Ime: <?php echo htmlspecialchars($pet->title); ?></h2>
                         <hr>
                         <div class="pet-short-text">
                             <p>Kratak opis:   <?php echo htmlspecialchars($pet->short_text); ?></p>  <br>  <br>
                         </div>
                          </div>
                       
                         <a class="button info-color" href="<?php echo Configuration::BASE; ?>pet/<?php echo $pet->pet_id; ?>">Opsirnije</a>
                  
                      </article>
                     <?php endforeach; ?>
                   
                </div>
            </div>
        </div>
       
    </div>
</article>




<?php include 'app/views/_global/afterContent.php'; ?>

