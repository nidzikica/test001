<?php include 'app/views/_global/beforeContent.php'; ?>


<article class="row  polariod">
    <div class="col-xs-12">

        <header>
            <h2 class="text-center color"><?php echo htmlspecialchars($DATA['pet']->title); ?></h2>
           
        </header>
        <br>
  
        <div class="more col-sm-12">
          
            <h4 class="text-center text-info"> Kratak opis:</h4> <h4>- <?php echo htmlspecialchars($DATA['pet']->short_text); ?></h4>
            <hr>
            <h4 class="text-primary text-center">  Detaljan opis:<br> </h4>  <p><?php echo htmlspecialchars($DATA['pet']->long_text); ?></p>



<hr>
        </div>

        
        <div class="col-sm-12 col-md-7">
            <div class="images">
                <?php foreach ($DATA['pet']->images as $image): ?>
                    <img src="<?php echo Configuration::BASE . $image->path; ?>" alt="<?php echo htmlspecialchars($DATA['pet']->title) . ' - ' . $image->image_id; ?>" class="img-responsive">
                    <hr>
                        <?php endforeach; ?>

            </div> </div>
      
<div class="details col-sm-12 col-md-5 text-center">

    <h4 class="text-info"> Kontakt: </h4>  <h4>  <b>   <span class="glyphicon glyphicon-envelope"></span>  <?php echo htmlspecialchars($DATA['pet']->users->email); ?> </b> </h4>        
            <div class="tags">
  <h4 class="text-info">Dodatne informacje:</h4>  
                <?php foreach ($DATA['pet']->tags as $tag): ?>
                     <h4><?php echo htmlspecialchars($tag->name); ?> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></h4>
                <?php endforeach; ?>
            </div>
        </div>


    </div>

</article>




<?php include 'app/views/_global/afterContent.php'; ?>
 