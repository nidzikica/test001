<?php include 'app/views/_global/beforeContent.php'; ?>
<article class="row">
    <div class="col-xs-12">
        <header class="text-center"> <h1 class="color">Registracija na sajt</h1><hr></header>
        <div class="page-content  polariod">
            <form method="post">
                <div class="form-group">
                    <label for="f1_username">Korisničko ime:</label><br>
                    <input type="text" name="username"   required class="form-control"
                           pattern="^[A-z0-9_\-\.]{4,32}$" placeholder="Unesite korisničko ime"><br>
                </div>

                <div class="form-group">
                    <label for="f1_email">E-mail:</label><br>
                    <input type="email" name="email" required class="form-control"
                            placeholder="Unesite vas mejl"><br>
                </div>

                <div class="form-group">
                    <label for="f1_password">Lozinka:</label><br>
                    <input type="password" name="password" required class="form-control"
                           pattern="^.{6,255}$" placeholder="Unesite lozinku"><br>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Registrujte se
                    </button>
                    <hr>
                    
                </div>
            </form>
            
        </div>
    </div>
    <?php if (isset($DATA['message'])): ?>
            <p><?php echo htmlspecialchars($DATA['message']);?></p>
            <?php endif;?>
</article>




<?php include 'app/views/_global/afterContent.php'; ?>