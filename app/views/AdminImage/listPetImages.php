<?php include 'app/views/_global/beforeContent.php'; ?>
 
<article class="row">
    <div class="col-xs-12">
        <header class="text-center">
            <h2>Spisak slika</h2><hr>
        </header>
        <div class="page-content">
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <td>
                            <?php Misc::url('images/pet/' . $DATA['pet_id'] . '/add' ,'Dodaj novu sliku');?>
                        </td>
                    </tr>
                    <tr>
                        <th>Id:</th>
                        <th>Slika:</th>
                        <th>Opcije:</th>
                       
                       
                    </tr>
                </thead> 
                <tbody>
                    <?php foreach ($DATA['images'] as $image):?>
                    <tr>
                        <td class="col-md-4"><?php echo $image->image_id; ?></td>
                        <td class="col-md-4">
                            <img src="<?php echo Configuration::BASE . htmlspecialchars($image->path); ?>" class="img-responsive">
                        </td>
                        <td class="col-md-4"><?php Misc::url('images/pet/' . $DATA['pet_id'] . '/delete/' .$image->image_id , 'Obrisi'); ?></td>
                       
                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>
</article>



<?php include 'app/views/_global/afterContent.php'; ?>

