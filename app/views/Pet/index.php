<?php include 'app/views/_global/beforeContent.php'; ?>
 

<article class="block">
    <header class="text-center color">
        <h1>Lista vasih ljubimaca</h1><hr>
    </header>
    <a class="button info-color" href="<?php echo Configuration::BASE; ?>pet/add/"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Dodaj na usvajanje</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Ime</th>
                <th>Kratak opis</th>
                <th colspan="3">Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($DATA['pets'] as $pet_id): ?>
            <tr>
                <th><?php echo $pet_id->pet_id; ?></th>
                <td><?php echo htmlspecialchars($pet_id->title); ?></td>
                <td><?php echo $pet_id->short_text; ?></td>
                
                 
                  <td>
                       <?php Misc::url('pet/edit/' . $pet_id->pet_id, 'Izmena'); ?>   </td>
                      <td>    <?php Misc::url('images/pet/' . $pet_id->pet_id, 'Slike'); ?>
                  </td>
                <td>
                       <?php Misc::url('pet/delete/' . $pet_id->pet_id, 'Obrisi'); ?>   </td>
                    
            <?php endforeach; ?>
        </tbody>
    </table>
</article>



<?php include 'app/views/_global/afterContent.php'; ?>


