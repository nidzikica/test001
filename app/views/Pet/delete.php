<?php include 'app/views/_global/beforeContent.php'; ?>
 

<article class="block">
    <header class="text-center">
        <h1 class="color">Brisanje oglasa za &quot;<?php echo htmlspecialchars($DATA['pet']->title); ?>&quot;</h1><hr>
    </header>
 <a class="button btn btn-primary" href="<?php echo Configuration::BASE; ?>pet/list/">Vasi oglasi</a>
  
 <form method="post" action="<?php echo Configuration::BASE; ?>pet/delete/<?php echo $DATA['pet']->pet_id; ?>">
        <input type="hidden" name="confirmed" value="1">
        <button type="submit" class="button info-color">Obrisite vas oglas</button>
    </form>
    
    <?php if(isset($DATA['message'])):?>
    <p><?php echo htmlspecialchars($DATA['message']);?></p>
    <?php endif; ?>
</article>

<?php include 'app/views/_global/afterContent.php'; ?>


