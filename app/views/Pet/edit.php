<?php include 'app/views/_global/beforeContent.php'; ?>
 <article class="row">
    <div class="col-xs-12">
        <h2 class="text-center color">Izmena ljubimca</h2><hr>
        <div class="page-content">
            <a class="margin"><?php Misc::url('pet/list','Svi ljubimci');?></a>
            <form method="post" class="edit polariod">
                
            <label for="title">Ime</label>
            <input class="form-control" type="text" name="title" id="name" required value="<?php echo htmlspecialchars($DATA['pet']->title); ?>"><br>
            
              <label for="short_text">Kratak opis:</label>
             <input class="form-control" type="text" name="short_text" id="short_text" required value="<?php echo htmlspecialchars($DATA['pet']->short_text); ?>"><br>
             
               <label for="long_text">Detaljan tekst</label>
               <textarea  class="form-control" name="long_text" id="long_text" required><?php echo htmlspecialchars($DATA['pet']->long_text); ?></textarea><br>
               
               <label for="pet_category_id">Vrsta</label>
               <select name="pet_category_id" id="pet_category_id">
                   <?php foreach ($DATA['categories']as $item):?>
                   <option class="form-control" value="<?php echo $item->pet_category_id; ?> " <?php if ($DATA['pet']->pet_category_id == $item->pet_category_id) echo 'selected';?>>
                       <?php echo htmlspecialchars($item->name);?></option>
                   
                   <?php endforeach; ?>
               </select><br>
             <br>
               <label for="race_id">Rasa</label>
               <select name="race_id" id="race_id">
                   <?php foreach ($DATA['races']as $item):?>
                   <option class="form-control" value="<?php echo $item->race_id; ?> " <?php if ($DATA['pet']->race_id == $item->race_id) echo 'selected';?>>
                       <?php echo htmlspecialchars($item->name); ?>
                   </option>
                   
                   <?php endforeach; ?>
               </select><br>
              
               <label>Dokumentacija:</label><br>
               <?php foreach ($DATA['tags'] as $tag): ?>
               <input type="checkbox" name="tag_ids[]" value="<?php echo $tag->tag_id; ?>"<?php if(in_array($tag->tag_id, $DATA['pet']->tag_ids)) echo 'checked'; ?>>
               <?php echo htmlspecialchars($tag->name);?><br>
               <?php endforeach; ?>
               <button class="btn info-color" type="submit">Izmeni</button>
            </form>
            
             
        </div>
          <?php if(isset($DATA['message'])):?>
                <p><?php echo htmlspecialchars($DATA['message']);?></p>
                <?php endif; ?>
    </div>
</article>

 



<?php include 'app/views/_global/afterContent.php'; ?>


