<?php include 'app/views/_global/beforeContent.php'; ?>
 
<article class="row">
    <div class="col-xs-12">
        <header class="text-center">
            <h2>Spisak svih rasa</h2><hr>
        </header>
        <div class="page-content">
            <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <td colspan="4"class="align-right">
                            <?php Misc::url('admin/categories/add','Dodaj novu vrstu');?>
                        </td>
                    </tr>
                    <tr>
                        <th>Id</th>
                        <th>Ime</th>
                        <th>Slug</th>
                       
                       
                    </tr>
                </thead> 
                <tbody>
                    <?php foreach ($DATA['categories'] as $category):?>
                    <tr>
                        <td><?php echo $category->pet_category_id; ?></td>
                        <td><?php echo htmlspecialchars($category->name); ?></td>
                        <td><?php echo htmlspecialchars($category->slug); ?></td>
                       
                        
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>
</article>



<?php include 'app/views/_global/afterContent.php'; ?>

