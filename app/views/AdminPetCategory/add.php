<?php include 'app/views/_global/beforeContent.php'; ?>
 
<article class="row">
      <div class="col-xs-12">
          <header class="text-center">    <h2>Dodavanje vrste</h2> <hr>
          </header>
        <div class="page-content polariod">
            <a><?php Misc::url('admin/categories','Sve vrstu');?></a>
            <form method="post"><br>
            <label  for="name">Ime vrste</label>
            <input  type="text" class="form-control" name="name" id="name" required><br>
            
             <label for="slug">Slug vrste</label>
             <input type="text" class="form-control" name="slug" id="slug" required pattern='[a-z0-9\-]+'><br>
             
             <button type="submit">Dodaj vrstu</button>
            </form>
            
               <?php if(isset($DATA['message'])):?>
                <p><?php echo htmlspecialchars($DATA['message']);?></p>
                <?php endif; ?>
        </div>
    </div>
</article>



<?php include 'app/views/_global/afterContent.php'; ?>

