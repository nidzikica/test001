<?php

/**
 * Ovo je osnovni kontroler aplikacije koji koristi za izvrsavanje 
 * zahteva upucenih prema podrazumevanim rutama koje poznaje veb sajt.
 */
class MainController extends Controller {

    /**
     * Ova je osnovni metod, pocenta stranica sajta
     */
    function index($page = 0) {
        $this->set('categories', PetCategoryModel::getAll());
        $this->set('races', RaceModel::getAll());
        $this->set('tags', TagModel::getAll());

        $pets = PetModel::getAllPaged($page);
        for ($i = 0; $i < count($pets); $i++) {
            $pets[$i]->images = PetModel::getPetImages($pets[$i]->pet_id);
        }
        $this->set('pets', $pets);
    }

    /**
     * Ovaj metod vraca oglas sa odredjenim id brojem i prikazuje tagove,slike,email,tekstove
     *  kao informacije o oglasu

     */
    public function petDetails($id) {
        $pet = PetModel::getById($id);
        if (!$pet) {
            Misc::redirect('');
        }
        $pet->tags = PetModel::getTagsForPetId($pet->pet_id);
        $pet->races = RaceModel::getById($pet->pet_id);
        $pet->images = PetModel::getPetImages($pet->pet_id);
        $pet->users = UserModel::getById($pet->user_id);

        $this->set('pet', $pet);
    }

    /**
     * Ovaj metod vraca spisak svih kategorija

     */
    public function listByCategory($categorySlug) {
        $category = PetCategoryModel::getBySlug($categorySlug);

        if (!$category) {
            Misc::redirect('');
        }
        $pets = PetModel::getPetsByPetCategoryId($category->pet_category_id);
        for ($i = 0; $i < count($pets); $i++) {
            $pets[$i]->images = PetModel::getPetImages($pets[$i]->pet_id);

            $pets[$i]->tags = PetModel::getTagsForPetId($pets[$i]->pet_id);
        }

        $this->set('pets', $pets);
        $this->set('category', $category);
    }

    /**
     * Ovaj metod vraca formular za dodavanje novih korisnika ako su podaci poslati HTTP POST metodom i redirektuje ga na login metod
     * @return void
     */
    public function registration() {
        if ($_POST) {
            $username = filter_input(INPUT_POST, 'username');
            $email = filter_input(INPUT_POST, 'email');
            $password = filter_input(INPUT_POST, 'password');
            if (preg_match('/^[A-z0-9_\-\.]{4,32}$/', $username) and preg_match('/^.{6,255}$/', $password)) {
                $passwordHash = hash('sha512', $password . Configuration::SALT);
                $user = UserModel::add($username, $email, $passwordHash);
                if ($user) {
                    Session::set('user_id', $user->user_id);
                    Session::set('username', $username);
                    Session::set('email', $email);
                    Session::set('user_ip', filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_FLAG_IPV4));
                    Session::set('user_agent', filter_input(INPUT_SERVER, 'HTTP_USER_AGENT'));
                    Misc::redirect('pet/list');
                } else {
                    $this->set('message', 'Username or password are incorrect or the user is not active');
                }
            } else {
                $this->set('message', 'Nisu dobri parametri logovanje');
            }
        }
    }

    /**
     * Ovaj metod vrsi validaciju, proverava postojanje korisnika sa pristiglim parametrima i ukoliko sve prodje bez greske
     * metod kreira sesiju za korisnika i preusmerava ga default rutu.
     * @return void
     */
    public function login() {
        if ($_POST) {
            $username = filter_input(INPUT_POST, 'username');
            $password = filter_input(INPUT_POST, 'password');
            if (preg_match('/^[A-z0-9_\-\.]{4,32}$/', $username) and preg_match('/^.{6,255}$/', $password)) {
                $passwordHash = hash('sha512', $password . Configuration::SALT);
                $user = UserModel::getActiveUserByUsernameAndPasswordHash($username, $passwordHash);

                if ($user) {
                    Session::set('user_id', $user->user_id);
                    Session::set('username', $username);
                    Session::set('user_ip', filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_FLAG_IPV4));
                    Session::set('user_agent', filter_input(INPUT_SERVER, 'HTTP_USER_AGENT'));
                    Misc::redirect('pet/list');
                } else {
                    $this->set('message', 'Username or password are incorrect or the user is not active');
                }
            } else {
                $this->set('message', 'Nisu dobri parametri logovanje');
            }
        }
    }

    /**
     * Ovaj metod gasi sesiju cime efektivno unistava sve sesije
     *  a zatim preusmerava korisnika na stranicu za prijavu korisnika
     */
    function logout() {
        Session::end();
        Misc::redirect('login');
    }

    function search() {
        if (!$_POST) {
            Misc::redirect('');
        }
        $race_id = filter_input(INPUT_POST, 'race_id', FILTER_SANITIZE_NUMBER_INT);
        $pet_category_id = filter_input(INPUT_POST, 'pet_category_id', FILTER_SANITIZE_NUMBER_INT);

        $tag_ids = filter_input(INPUT_POST, 'tag_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

        $pets = PetModel::homePageSearch($race_id, $pet_category_id, $tag_ids);

        $this->set('categories', PetCategoryModel::getAll());
        $this->set('races', RaceModel::getAll());
        $this->set('tags', TagModel::getAll());

        for ($i = 0; $i < count($pets); $i++) {
            $pets[$i]->images = PetModel::getPetImages($pets[$i]->pet_id);
        }
        $this->set('pets', $pets);
    }

}
