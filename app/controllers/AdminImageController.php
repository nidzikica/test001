<?php

/**
 * Klasa kontrolera  admin panela aplikacije za rad sa slikama
 */
class AdminImageController extends AdminController {

    public function index() {
        
    }

    /**
     * listPetImages metod admin kotrolera za rad sa slikama vraca spisak svih slika sa zadatim id brojem
     */
    public function listPetImages($pet_id) {

        $this->set('images', ImageModel::getByPetId($pet_id));
        $this->set('pet_id', $pet_id);
    }

    /**
     * Upload metod admin kotrolera za rad sa slikama izvrsava dodavanje novih slika za zadati oglas
     * vrsi njegovu proveru i dodeljuje ime 
     */
    public function uploadImage($pet_id) {
        $this->set('pet_id', $pet_id);

        if (!$_FILES or ! isset($_FILES['image']))
            return;

        if ($_FILES['image']['error'] != 0) {
            $this->set('message', 'Doslo je do greske prilikom dodavanja slike');
            return;
        }
        $temporaryPath = $_FILES['image']['tmp_name'];
        $fileSize = $_FILES['image']['size'];
        $originalName = $_FILES['image']['name'];

        if ($fileSize > 1000 * 1024) {
            $this->set('message', 'Fajl mora biti do 1MB');
            return;
        }
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($temporaryPath);
        if ($mimeType != 'image/jpeg') {
            $this->set('message', 'GRESKA! Slika mora da ima extenziju JPG!');
            return;
        }


        $baseName = strtolower($originalName);
        $baseName = preg_replace('[^a-z0-9\- ]', '', $baseName);


        $fileName = date('YmdHisu') . '-' . $baseName . '.jpg';

        $newLocation = Configuration::IMAGE_DATA_PATH . $fileName;

        $res = move_uploaded_file($temporaryPath, $newLocation);

        if (!$res) {
            $this->set('message', 'Doslo je do greske prilikom cuvanja fajlova na krajnu lokaciju');
            return;
        }

        $res = ImageModel::add($newLocation, $pet_id);
        if (!$res) {
            $this->set('message', 'Doslo je do greske prilikom upisa u bazu.');
            return;
        }
        Misc::redirect('images/pet/' . $pet_id);
    }

}
