<?php

/**
 * Klasa kontrolera  admin panela aplikacije za rad sa tagovima
 */
class AdminTagController extends AdminController {

    /**
     * Indeks metod admin kotrolera za rad sa tagovima vraca spisak svih tagova
     */
    public function index() {
        $this->set('tags', TagModel::getAll());
    }

}
