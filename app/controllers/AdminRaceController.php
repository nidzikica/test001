<?php

/**
 * Klasa kontrolera  admin panela aplikacije za rad sa rasama
 */
class AdminRaceController extends AdminController {

    /**
     * Indeks metod admin kotrolera za rad sa rasama vraca spisak svih rasa
     */
    public function index() {
        $this->set('races', RaceModel::getAll());
    }

    /**
     * Ovaj metod vraca formular za dodavanje novih rasa ako su podaci poslati HTTP POST metodom
     */
    public function add() {
        if (!$_POST)
            return;
        $name = filter_input(INPUT_POST, 'name');
        $slug = filter_input(INPUT_POST, 'slug');

        $race_id = RaceModel::add($name, $slug);
        if ($race_id) {
            Misc::redirect('admin/races/');
        } else {
            $this->set('message', 'Doslo je do grekse prilikom dodavanja rase');
        }
    }

}
