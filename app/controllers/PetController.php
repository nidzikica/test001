<?php

/**
 * Klasa kontrolera  admin panela aplikacije za rad sa oglasima
 */
class PetController extends AdminController {

    /**
     * Indeks metod admin kotrolera za rad sa ljubimcima vraca spisak svih ljubimaca sa ulogovanim korisnikom na sesiju
     */
    public function index() {
        $pets = PetModel::getByUserId(Session::get('user_id'));
        $this->set('pets', $pets);
    }

    /**
     * Ovaj metod vraca formular za dodavanje novih ljubimaca ako su podaci poslati HTTP POST metodom
     * @return void
     */
    public function add() {
        $this->set('races', RaceModel::getAll());
        $this->set('categories', PetCategoryModel::getAll());
        $this->set('tags', TagModel::getAll());
        if (!$_POST)
            return;
        $title = filter_input(INPUT_POST, 'title');
        $short_text = filter_input(INPUT_POST, 'short_text');
        $long_text = filter_input(INPUT_POST, 'long_text');

        $pet_category_id = filter_input(INPUT_POST, 'pet_category_id', FILTER_SANITIZE_NUMBER_INT);

        $race_id = filter_input(INPUT_POST, 'race_id', FILTER_SANITIZE_NUMBER_INT);
        if (preg_match('/^([A-z0-9]+ ){50,800}$/', $long_text) and ! preg_match('/^(([a-z0-9/./-:]+\.)[a-z]{2,6})$/', $long_text)) {

            $pet_id = PetModel::add($title, $short_text, $long_text, $pet_category_id, $race_id, Session::get('user_id'));

            if ($pet_id) {
                $tag_ids = filter_input(INPUT_POST, 'tag_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);


                foreach ($tag_ids as $tag_id) {
                    PetModel::addTagToPet($pet_id, $tag_id);
                }

                Misc::redirect('pet/list');
            } else {
                $this->set('message', 'Doslo je do greske prilikom dodavanja');
            }
        } else {
            $this->set('message', 'Detaljan opis mora biti duzine od 50 do 800 reci ili sadrzi link !');
        }
    }

    /**
     * Ovaj metod vraca formular za izmenu postojecih ljubimaca ako su podaci poslati HTTP POST metodom
     * @return void
     */
    public function edit($id) {
        $this->set('races', RaceModel::getAll());
        $this->set('categories', PetCategoryModel::getAll());
        $this->set('tags', TagModel::getAll());
        $pet = PetModel::getById($id);

        if (!$pet) {
            Misc::redirect('pet/list');
        }
        $pet->tags = PetModel::getTagsForPetId($id);
        $pet->tag_ids = [];

        foreach ($pet->tags as $tag) {
            $pet->tag_ids[] = $tag->tag_id;
        }

        $this->set('pet', $pet);

        if (!$_POST)
            return;

        $title = filter_input(INPUT_POST, 'title');
        $short_text = filter_input(INPUT_POST, 'short_text');
        $long_text = filter_input(INPUT_POST, 'long_text');
        $pet_category_id = filter_input(INPUT_POST, 'pet_category_id', FILTER_SANITIZE_NUMBER_INT);

        $race_id = filter_input(INPUT_POST, 'race_id', FILTER_SANITIZE_NUMBER_INT);

        $res = PetModel::edit($id, $title, $short_text, $long_text, $race_id, $pet_category_id);


        if (preg_match('/^([A-z]+ ){50,800}$/', $long_text) and ! preg_match('/^(([a-z0-9/./-:]+\.)[a-z]{2,6})$/', $long_text)) {
            $tag_ids = filter_input(INPUT_POST, 'tag_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

            PetModel::deleteAllTags($id);
            foreach ($tag_ids as $tag_id) {
                PetModel::addTagToPet($id, $tag_id);
            }

            if ($res) {
                Misc::redirect('pet/list');
            } else {
                $this->set('message', 'Doslo je do greske prilikom izmene podataka');
            }
        } else {
            $this->set('message', 'Detaljan opis mora biti duzine od 50 do 800 reci!');
        }
    }

    /**
     * Ovaj metod vraca formular za brisanje ljubimaca ako su podaci poslati HTTP POST metodom 
     * @return void
     */
    public function delete($id) {
        $pet = PetModel::getById($id);


        if ($_POST) {
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_NUMBER_INT);
            if ($confirmed == 1) {
                $res = PetModel::delete($id);
                if ($res) {
                    Misc::redirect('pet/list');
                } else {
                    $this->set('message', 'Could not delete this product!');
                }
            }
        }
        $pet = PetModel::getById($id);
        $this->set('pet', $pet);
    }

}
