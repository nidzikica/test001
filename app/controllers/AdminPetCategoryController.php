<?php

/**
 * Klasa kontrolera  admin panela aplikacije za rad sa kategorijama
 */
class AdminPetCategoryController extends AdminController {

    /**
     * Indeks metod admin kotrolera za rad sa kategorijama vraca spisak svih kategorija
     */
    public function index() {
        $this->set('categories', PetCategoryModel::getAll());
    }

    /**
     * Ovaj metod vraca formular za dodavanje novih kategorija ako su podaci poslati HTTP POST metodom
     */
    public function add() {
        if (!$_POST)
            return;
        $name = filter_input(INPUT_POST, 'name');
        $slug = filter_input(INPUT_POST, 'slug');

        $category_id = PetCategoryModel::add($name, $slug);
        if ($category_id) {
            Misc::redirect('admin/categories/');
        } else {
            $this->set('message', 'Doslo je do grekse prilikom dodavanja rase');
        }
    }

}
