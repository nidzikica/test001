<?php
/**
 * Metod koji odgovara tabeli image
  */
class ImageModel implements ModelInterface{
    
/**
 * Metod vraca spisak svih slika poredjnaih po id broju 
 * @return array
 */
 public static function getAll() {
        $SQL = 'SELECT * FROM image;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
       
    }
/**
 * Metod vraca objekat sa podacima slike cije je id dat kao argument 
 * @param int id
 * @return stdClass|NULL
 */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM image WHERE image_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
    /**
 * Metod vraca objekat sa podacima slike cije je id dat kao argument id iz tabele pet
 * @param int id
 * @return stdClass|NULL
 */
    public static function getByPetId($id){
        $id = intval($id);
        $SQL = 'SELECT * FROM image WHERE pet_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
    /**
 * Metod dodaje objekat-sliku za odredjeni id iz tabele pet 
 * @param int id,path
 * @return stdClass|NULL
 */
    public static function add($path, $pet_id){
        $SQL = 'INSERT INTO image(path, pet_id) VALUES (?,?);';
        $prep= DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$path, $pet_id]);
    }
           
}
