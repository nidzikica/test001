<?php
/**
 * Metod koji odgovara tabeli tag
  */
class TagModel implements ModelInterface{
    /**
 * Metod vraca spisak svih tagova iz tabele tag poredjnaih po imenu 
 * @return array
 */
 public static function getAll() {
        $SQL = 'SELECT * FROM tag ORDER BY name;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
        
    }
/**
 * Metod vraca objekat sa podacima za tag_id cije je id dat kao argument 
 * @param int id
 * @return stdClass|NULL
 */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM tag WHERE tag_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
       
    }
    /**
 * Metod vraca listu sa podacima vezanim za id cije je id dat kao argument u tabeli pet_tag 
 * @param int id
 * @return stdClass|NULL
 */
     public static function getPetSForTagId($id){
        $id= intval($id);
        $SQL = 'SELECT * FROM pet_tag WHERE tag_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$tag_id]);
        $spisak = $prep->fetchAll(PDO::FETCH_OBJ);
        $list=[];
        foreach ($spisak as $item){
            $list[] = PetModel::getById($item->pet_id);
            
        }
        return $list;
    }
}
