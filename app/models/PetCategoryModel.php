<?php
/**
 * Metod koji odgovara tabeli pet_cateogy
  */
class PetCategoryModel implements ModelInterface{
    /**
 * Metod vraca spisak svih kategorija poredjnaih po imenu 
 * @return array
 */
 public static function getAll() {
        $SQL = 'SELECT * FROM pet_category ORDER BY `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
      
    }
/**
 * Metod vraca objekat sa podacima kategorije cije je id dat kao argument 
 * @param int id
 * @return stdClass|NULL
 */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM pet_category WHERE pet_category_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
        
    }
    /**
 * Metod vraca objekat sa podacima kategorije cije je slug dat kao argument 
 * @param sring slug
 * @return stdClass|NULL
 */
    public static function getBySlug($slug) {
        $SQL = 'SELECT * FROM pet_category WHERE slug = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$slug]);
        return $prep->fetch(PDO::FETCH_OBJ);
        
    }
  /**
 * Metod dodaje objekat-lategprok za odredjeni sa odredjenim imenom i slugom 
 * @param string name,slug
 * @return stdClass|NULL
 */
    public static function add($name,$slug){
    $SQL = 'INSERT INTO pet_category (name, slug) VALUES (?, ?);';
    $prep = DataBase::getInstance()->prepare($SQL);
    $res = $prep->execute([$name,$slug]);
    if($res){
        return DataBase::getInstance()->lastInsertId();
    }else{
        return false;
    }
        }
  
}
