<?php
 /**
 * Metod koji odgovara tabeli race
  */
class RaceModel implements ModelInterface {
/**
 * Metod vraca spisak svih rasa iz tabele racepet poredjnaih po imenu 
 * @return array
 */
    public static function getAll() {
        $SQL = 'SELECT * FROM race ORDER BY `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
/**
 * Metod vraca objekat sa podacima za race_id cije je id dat kao argument 
 * @param int id
 * @return stdClass|NULL
 */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM race WHERE race_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
/**
 * Metod dodaje novu rasu sa podacima u tabelu race 
 * @param int pet_category_id
 * string name,slug
 * @return stdClass|NULL
 */
    public static function add($name, $slug, $pet_category_id) {
        $SQL = 'INSERT INTO race (name, slug,pet_category_id) VALUES (?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $slug, $pet_category_id]);
        if ($res) {
            return DataBase::getInstance()->lastInsertId();
        } else {
            return false;
        }
    }

    public static function getRaceByCategory($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM race WHERE pet_category_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

}
