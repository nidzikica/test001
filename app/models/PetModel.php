<?php
 /**
 * Metod koji odgovara tabeli pet
  */
class PetModel implements ModelInterface {
   
/**
 * Metod vraca spisak svih oglasa iz tabele pet poredjnaih po imenu 
 * @return array
 */
    public static function getAll() {
        $SQL = 'SELECT * FROM pet ORDER BY title;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public static function getAllPaged($page) {
        $page = max(0, $page);
        $first = $page * Configuration::ITEM_PER_PAGE;
        $SQL = 'SELECT * FROM pet ORDER BY `pet_id` DESC LIMIT ?,?';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEM_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
/**
 * Metod vraca objekat sa podacima za pet_id cije je id dat kao argument 
 * @param int id
 * @return stdClass|NULL
 */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM pet WHERE pet_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
/**
 * Metod vraca spisak svih objekata iz tabele pet gde je user_id dat kao argument 
 * @return array
 */
    public static function getByUserId($user_id) {
        $user_id = intval($user_id);
        $SQL = 'SELECT * FROM pet WHERE user_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$user_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
/**
 * Metod vraca listu svih tagova iz tabele pet_tag gde je id iz tabele pet dat kao argument 
 * @return array
 */
    public static function getTagsForPetId($pet_id) {
        $pet_id = intval($pet_id);
        $SQL = 'SELECT * FROM pet_tag WHERE pet_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$pet_id]);
        $spisak = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($spisak as $item) {
            $list[] = TagModel::getById($item->tag_id);
        }
        return $list;
    }
 /** Metod brise objekat sa podacima iz pet tabele cije je id dat kao argument id iz tabele pet
 * @param int id
 * @return stdClass|NULL
 */
       public static function delete($pet_id) {
        $pet_id = intval($pet_id);
        $SQL = 'DELETE FROM pet WHERE pet_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$pet_id]);
    }
   /** Metod dodaje objekat sa podacima u pet tabelu 
 * @param string title,short_text,long_text
    * int pet_category_id,race_id,user_id
 * @return stdClass|NULL
 */ 
    public static function add($title, $short_text, $long_text, $pet_category_id, $race_id, $user_id) {
        $SQL = 'INSERT INTO pet(`title`, `short_text`, `long_text` , `pet_category_id`, `race_id`, `user_id`) VALUE (?, ?, ?, ?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$title, $short_text, $long_text, $pet_category_id, $race_id, $user_id]);
        if ($res) {
            return DataBase::getInstance()->lastInsertId();
        } else {
            return false;
        }
    }
/**
 * Metod vraca spisak svih objekata iz tabele pet gde je pet_cateogry_id dat kao argument 
 * @param int pet_category_id
 * @return array
 */
    public static function getPetsByPetCategoryId($pet_category_id) {
        $SQL = 'SELECT * FROM pet WHERE pet_category_id = ? ORDER BY `title`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$pet_category_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
/**
 * Metod menja podatke iz tabele pet za objekat sa datim id brojem 
 * @param string title,short_text,long_text
    * int pet_category_id,race_id,user_id
 * @return string title,short_text,long_text
    * int pet_category_id,race_id,user_id
 */
    public static function edit($id, $title, $short_text, $long_text, $pet_categorty_id, $race_id) {
        $SQL = 'UPDATE pet SET title=?,short_text=?,long_text=?,pet_category_id=?,race_id=? WHERE pet_id=?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$title, $short_text, $long_text, $pet_categorty_id, $race_id, $id]);
    }
/**
 * Metod dodaje tag_id objektu pet_id u tabeli pet_tag
 * @param int pet_id, tag_id
 * @return int stdClass|NULL
 */
    public static function addTagToPet($pet_id, $tag_id) {
        $SQL = 'INSERT INTO pet_tag (pet_id, tag_id) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$pet_id, $tag_id]);
    }
/**
 * Metod vraca sve objekte-slike od datog objekta sa zadatim id brojem iz tabele image
 * @param int pet_id
 * @return int stdClass|NULL
 */
    public static function getPetImages($pet_id) {
        $SQL = 'SELECT * FROM image WHERE pet_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$pet_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
 /** Metod brise sve tagove sa zadatim pet_id brojem iz tabele pet_tag
 * @param int id
 * @return stdClass|NULL
 */
    public static function deleteAllTags($pet_id) {
        $SQL = 'DELETE FROM pet_tag WHERE pet_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$pet_id]);
    }

    public static function homePageSearch($race_id, $pet_category_id, $tag_ids) {
        if ($race_id != -1 and $pet_category_id != -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i<count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE race_id = ? AND pet_category_id = ? AND pet_id IN ('
                    . 'SELECT pet_id'
                    . 'FROM pet_tag'
                    . 'WHERE tag_id IN(' . $tag_placeholder_string . ')'
                    . ')'
                    . ' ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $race_id, 
                $pet_category_id
            ];
            $niz = array_merge($niz, $tag_ids);
            $prep->execute($niz);
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
        if ($race_id == -1 and $pet_category_id != -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE pet_category_id = ? AND pet_id IN ('
                    . 'SELECT pet_id'
                    . 'FROM pet_tag'
                    . 'WHERE tag_id IN(' . $tag_placeholder_string . ')) ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $pet_category_id
            ];
            $niz = array_merge($niz, $tag_ids);
            $prep->execute($niz);
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
        if ($race_id != -1 and $pet_category_id == -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE race_id = ? AND pet_id IN ('
                    . 'SELECT pet_id'
                    . 'FROM pet_tag'
                    . 'WHERE tag_id IN(' . $tag_placeholder_string . ')) ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $race_id
            ];
            $niz = array_merge($niz, $tag_ids);
            $prep->execute($niz);
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
          if ($race_id == -1 and $pet_category_id == -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE  pet_id IN ('
                    . 'SELECT pet_id'
                    . 'FROM pet_tag'
                    . 'WHERE tag_id IN(' . $tag_placeholder_string . ')) ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
           
            $prep->execute( $tag_ids );
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
          if ($race_id != -1 and $pet_category_id != -1 and count($tag_ids) == 0) {
            
            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE race_id = ? AND pet_category_id = ? ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $race_id, 
                $pet_category_id
            ];
            $prep->execute($niz);
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
         if ($race_id == -1 and $pet_category_id != -1 and count($tag_ids) == 0) {
            
            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE pet_category_id = ? '
                    . 'ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                 $pet_category_id
            ];           
            $prep->execute($niz);
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
          if ($race_id != -1 and $pet_category_id == -1 and count($tag_ids) == 0) {
            
            $SQL = 'SELECT * '
                    . 'FROM pet'
                    . 'WHERE race_id = ? ORDER BY `pet_id` DESC';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                 $race_id
            ];
            $prep->execute($niz);
            return $prep->fetchALL(PDO::FETCH_OBJ);
        }
          if ($race_id == -1 and $pet_category_id == -1 and count($tag_ids) == 0) {
            
              return self::getAll();
        }
    }

}
