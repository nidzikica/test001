<?php
 /**
 * Metod koji odgovara tabeli user
  */
class UserModel implements ModelInterface {
    /**
 * Metod vraca spisak svih user-a iz tabele user poredjnaih po username-u 
 * @return array
 */
    public static function getAll() {
        $SQL = 'SELECT * FROM user ORDER BY username;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
    /**
 * Metod vraca objekat sa podacima za user_id cije je id dat kao argument 
 * @param int id
 * @return stdClass|NULL
 */
    public static function getById($user_id) {
        $user_id= intval($user_id);
        $SQL = 'SELECT * FROM user WHERE user_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$user_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
       /**
 * Metod vraca aktivne korisnike koji su ulogovani sa argumentima usenrame i passwordHash
 * @param string username,passwordHas
 * @return stdClass|NULL
 */
     public static function getActiveUserByUsernameAndPasswordHash($username, $passwordHash) {
        $SQL = 'SELECT * FROM user WHERE `username` = ? AND `password` = ? AND active = 1;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$username, $passwordHash]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
        /**
 * Metod dodaje novog user-a sa novim argumentima username,email,passwordHash
 * @param string username,email,passwordHas
 * @return stdClass|NULL
 */  
     public static function add($username,$email, $passwordHash){
         $SQL = 'INSERT INTO user(`username`,`email`,`password`) VALUES(?,?,?);';
         $prep = DataBase::getInstance()->prepare($SQL);
         return $prep->execute([$username,$email, $passwordHash]);
    }
}